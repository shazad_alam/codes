# Description


All my recent works are here. You will find Explanaratory Data Analysis, Predictive Analysis, my bachelor thesis and my masters thesis in here. To pull the projects from the repository:

```
cd existing_repo
git remote add origin https://gitlab.com/shazad_alam/codes.git
git branch -M main
git push -uf origin main
```

## Exploratory Data Analysis

- [ ] [EDA](https://gitlab.com/shazad_alam/codes/-/tree/main/EDA) - Here you will find the exploratory data analysis.

## Predictive Analysis
- [ ] [Gameof11](https://gitlab.com/shazad_alam/codes/-/tree/main/go11ai) - Predictive analysis for a fantasy sports platform. As a sports platform, cricket and football matches were published and contests were given. In here, the number of contests were predicted based on the teams. Separate scoring system implemented to rate out the teams from strong to weak team. 

## Bachelor's Thesis
- [ ] [Independent Component Analysis](https://gitlab.com/shazad_alam/codes/-/tree/main/neural_network) - Thesis topis is "Independent Component Analysis-based Initialization of Convolutional Neural Networks". Where Cifar-10 dataset were used. In pre processing, random feature were selected. Primarily used CNN to classify the random features. And finally ICA were used to predict the components from the results. 

## Master's Thesis
- [ ] [Modified audio signal](https://gitlab.com/shazad_alam/codes/-/tree/main/thesis) - Thesis is ongoing. The topic is "Accuracy Evaluation of Convolutional Neural Network Architecture by using modified audio
signals". After finishing the final evaluation, repository will be updated. 


